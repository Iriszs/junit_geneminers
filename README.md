# README #

This is a class design with JUnit tests for the Geneminers assignment.

### What is this repository for? ###

* Handing in the Project
* Version control

### How do I get set up? ###

* Download this project.
* Run the different test classes in src/test/nl/bioinf/geneminers/model/. The output shows if a test is passed or not.

### Who do I talk to? ###

* Iris Gorter
* Maaike Brummer