package nl.bioinf.geneminers.model;

import java.io.File;
import java.util.HashMap;

/**
 * Created by mlbrummer on 9-3-17.
 */
public class FileContentGetter {
    private HashMap<String,HashMap<String,Integer>> filecontent;
    private HashMap<String, Integer> experiment;

    public HashMap<String, HashMap<String,Integer>> openFile(){
        DbSaver dbsaver = new DbSaver();
        dbsaver.getInputFile();
        //lezen van de inputfile
        //maken van een hashmap met daarin een hashmap: {gen:{experiment:fkpmvalue}}
        //per regel/gen {experiment:fkpmvalue}

        experiment = new HashMap<>();
        experiment.put("WT1", 6376);
        experiment.put("WT2", 8382);
        filecontent.put("BSU00010",experiment);

        experiment = new HashMap<>();
        experiment.put("WT1", 4033);
        experiment.put("WT2", 6400);
        filecontent.put("BSU00020",experiment);

        return filecontent;
    }
}
