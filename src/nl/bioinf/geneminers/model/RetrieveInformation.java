package nl.bioinf.geneminers.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by mlbrummer on 9-3-17.
 */
public class RetrieveInformation {
    File inputFile;
    Path outputdir;
    String outputname;
    List<String> history;

    public File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) throws FileNotFoundException {
        if(!inputFile.exists()){
            throw new FileNotFoundException("File does not exist");
        }
        if(inputFile.isDirectory()){
            throw new FileNotFoundException("This is not a file, but a directory");
        }
        this.inputFile = inputFile;
    }

    public void setOutputName(String outputName) {
        this.outputname = outputName;
    }

    public void setOutputdir(Path outputdir){
        if (outputdir == null) {
            throw new NullPointerException(outputdir.toString() + "is null");
        }
        if (!Files.isDirectory(outputdir)) {
            throw new InvalidPathException(outputdir.toString(), "is not a directory");
        }
        if (Files.notExists(outputdir)) {
            throw new InvalidPathException(outputdir.toString(), "bestaat niet");
        }
        this.outputdir = outputdir;
    }

    public List<String> getHistory() {
        DbSaver dbsaver = new DbSaver();
        dbsaver.getHistory();
        return history;
    }
    public void setHistory() {
        DbSaver dbsaver = new DbSaver();
        dbsaver.saveHistory();
    }


}
