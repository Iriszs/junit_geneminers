package nl.bioinf.geneminers.model;

import java.util.List;

/*
Class that creates a matrix out of a list of fkpm values
and sets the matrix to save it in the database
 */

public class MatrixCreator extends FKPMValueGetter{
    List<String> history;
    Matrix matrix;


    public static Integer [][] createMatrix(List fkpmValues) {
        //Create a legit matrix for a sunny day scenario
        Integer [][] created_matrix = {{1, 2, 3},{4, 5, 6}};

        return created_matrix;

    }
    /*
    Because we don't need to implement fully functional code, different methods are written
    for every test. Normally one method would do the thing.
     */

    public static Integer [][] createMatrixNull(List fkpmValues){
        //Create an empty matrix when the list of fkpm values is empty
        Integer [][] created_matrix = {{}};
        //matrix has a length of 1 when nothing is added to it.
        if (created_matrix.length == 1) {
            //throw illegal argument exception
            throw new IllegalArgumentException("matrix can not be null");

        }

        return created_matrix;
    }

    public static Integer [][] createMatrixUnEqual(List fkpmValues){
        //create unequal matrix
        Integer [][] created_matrix = {{1, 2, 3},{4, 5}};
        //throws index out of bounds exception when the matrix is unequal
        if (created_matrix[1] != created_matrix[2]){
            throw new ArrayIndexOutOfBoundsException("Created matrix is unequal!");
        }
        return created_matrix;
    }

    public void setMatrix() {
        DbSaver dbsaver = new DbSaver();
        dbsaver.saveMatrix(matrix);
    }
}
