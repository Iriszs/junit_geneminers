package nl.bioinf.geneminers.model;

import java.awt.*;

/**
 * Created by mlbrummer on 9-3-17.
 */
public class HeatmapCreator extends MatrixCreator {
    Image image;
    public Image createHeatmap(Matrix matrix) {
        DbSaver dbsaver = new DbSaver();
        dbsaver.setHeatmap(image);

        return image;
    }
}
