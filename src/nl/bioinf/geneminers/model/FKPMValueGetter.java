package nl.bioinf.geneminers.model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mlbrummer on 9-3-17.
 */
public class FKPMValueGetter extends FileContentGetter {
    private List<Integer> fkpmValues = new ArrayList<Integer>();

    public List getFKPMValues(HashMap<String, HashMap<String,Integer>> filecontent) {
        //{gen:{experiment:fkpmvalue}}

        if (fkpmValues.isEmpty()) {
            throw new IllegalArgumentException("fkpmValues cannot be empty");
        }
        for (HashMap<String, Integer> v : filecontent.values()){
            for (int fkpm : v.values()) {
                fkpmValues.add(fkpm);
            }
        }
        return fkpmValues;
    }
}
