package nl.bioinf.geneminers.model;

import java.awt.*;
import java.io.File;
import java.util.List;

/**
 * Created by mlbrummer on 9-3-17.
 */
public class DbSaver {
    private List<String> history;
    private Matrix historyMatrix;
    private File inputFile;

    public void dbConnector() {

    }

    public void saveMatrix(Matrix matrix){

    }

    public void saveHistory() {
        dbConnector();
    }

    public Matrix getHistoryMatrix() {
        return historyMatrix;
    }

    public List<String> getHistory() {
        return history;
    }

    public void setInputFile(File inputFile){

    }

    public File getInputFile() {
        return inputFile;
    }

    public void setHeatmap(Image image){

    }

}
