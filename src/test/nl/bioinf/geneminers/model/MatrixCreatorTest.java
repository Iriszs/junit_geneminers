package test.nl.bioinf.geneminers.model;

import nl.bioinf.geneminers.model.MatrixCreator;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/*
This class test a sunnyday scenario and two scenario's that can go wrong.
 */


public class MatrixCreatorTest {

    @Test
    /*
    Sunny day scenario
     */
    public void createMatrixSunny(){
        //Create a legit fkpmValues list
        List <Integer> given_fkpmValues = new ArrayList<>();
        given_fkpmValues.add(1);
        given_fkpmValues.add(2);
        given_fkpmValues.add(3);
        given_fkpmValues.add(4);
        given_fkpmValues.add(5);
        given_fkpmValues.add(6);
        //Expected matrix based on the fkpm values list
        Integer [][] expected_matrix = {{1, 2, 3},{4, 5, 6}};
        Integer [][] returned_matrix_sunny = MatrixCreator.createMatrix(given_fkpmValues);
        //Check if the expected matrix and the matrix returned by the createMatrix function are the same
        //If they are the same, the test passes.
        assertEquals(expected_matrix, returned_matrix_sunny);

    }

    @Test(expected=IllegalArgumentException.class)
    /*
    This test expects a Illegal Argument Exception.
    This exception is thrown when a empty fkpm values list is added
    A empty matrix will be created. The empty matrix throws an IllegalArgument Exception
     */
    public void createMatrixNull(){
        //Create empty fkpm values list
        List <Integer> null_fkpmValues = new ArrayList<>();
        //execute function with the empty list
        MatrixCreator.createMatrixNull(null_fkpmValues);
    }

    @Test(expected=ArrayIndexOutOfBoundsException.class)
    /*
    This test expects a Array Index Out Of Bounds Exception
    This exception is thrown because the matrix that will be created
    is unequal.
     */
    public void createMatrixUnEqual(){
        //Create an unequal list with fkpm values
        List <Integer> unEqual_fkpmValues = new ArrayList<>();
        unEqual_fkpmValues.add(1);
        unEqual_fkpmValues.add(2);
        unEqual_fkpmValues.add(3);
        unEqual_fkpmValues.add(4);
        unEqual_fkpmValues.add(5);
        //execute function with an unequal list what can't create a matrix
        //and throws a array index out of bounds exception
        MatrixCreator.createMatrixUnEqual(unEqual_fkpmValues);

    }
}
