package test.nl.bioinf.geneminers.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

import nl.bioinf.geneminers.model.RetrieveInformation;
import org.junit.Assert;
import org.junit.Test;

public class RetrieveInformationTest {
    public RetrieveInformationTest() {
    }

    /**
     * Testmethode voor setInputFile. Hier wordt voldaan aan de eisen.
     * @throws FileNotFoundException
     */
    @Test
    public void setInputFileSunnyday() throws FileNotFoundException {
        RetrieveInformation retrieveInformation = new RetrieveInformation();
        File inputfile = new File("/home/maaike/test.txt");
        retrieveInformation.setInputFile(inputfile);
    }

    /**
     * Testmethode voor setInputFile. Hier wordt een file meegegeven dat niet bestaat.
     * @throws FileNotFoundException
     */
    @Test(expected = FileNotFoundException.class)
    public void setInputFileFileDoesNotExist() throws FileNotFoundException {
        RetrieveInformation retrieveInformation = new RetrieveInformation();
        File file = new File("/home/maaike/bestaatNiet.txt");
        retrieveInformation.setInputFile(file);
    }

    /**
     * Testmethode voor setInputFile. Hier wordt een directory meegegeven ipv een file.
     * @throws FileNotFoundException
     */
    @Test(expected = FileNotFoundException.class)
    public void setInputFileFileIsDir() throws FileNotFoundException {
        RetrieveInformation retrieveInformation = new RetrieveInformation();
        File file = new File("/home/maaike/");
        retrieveInformation.setInputFile(file);
    }

    //Nog een test met als file leeg is?

    /**
     * Testmethode voor setOutputdir. Hier wordt voldaan aan de eisen.
     */
    @Test
    public void setOutputdirSunnyday() {
        RetrieveInformation retrieveInformation = new RetrieveInformation();
        Path outputdir = Paths.get("/home/maaike/");
        retrieveInformation.setOutputdir(outputdir);
    }

    /**
     * Testmethode voor setOutputdir. Hier wordt een file meegegeven ipv een path.
     */
    @Test(expected = InvalidPathException.class)
    public void setOutputdirIsFile() {
        RetrieveInformation retrieveInformation = new RetrieveInformation();
        Path outputdir = Paths.get("/home/maaike/test.txt");
        retrieveInformation.setOutputdir(outputdir);
    }

    /**
     * Testmethode voor setOutputdir. Hier wordt null meegegeven ipv een path.
     */
    @Test(expected = NullPointerException.class)
    public void setOutputdirIsNull() {
        RetrieveInformation retrieveInformation = new RetrieveInformation();
        Path outputdir = Paths.get(null);
        retrieveInformation.setOutputdir(outputdir);

    }

    /**
     * Testmethode voor setOutputdir. Hier wordt een path meegegeven dat niet bestaat.
     */
    @Test(expected = InvalidPathException.class)
    public void setOutputdirDoesNotExist() {
        RetrieveInformation retrieveInformation = new RetrieveInformation();
        Path outputdir = Paths.get("/zomaariets/");
        retrieveInformation.setOutputdir(outputdir);
    }









    @Test
    public void setoutputdirSunnyday() throws NoSuchFieldException, IllegalAccessException {
        RetrieveInformation retrieveInformation = new RetrieveInformation();
        retrieveInformation.setOutputName("test");
        Field field = retrieveInformation.getClass().getDeclaredField("outputName");
        field.setAccessible(true);
        Assert.assertEquals("Fields didn\'t match", field.get(retrieveInformation), "test");
    }
}
