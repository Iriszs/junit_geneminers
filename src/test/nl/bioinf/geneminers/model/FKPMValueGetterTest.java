package test.nl.bioinf.geneminers.model;

import nl.bioinf.geneminers.model.FKPMValueGetter;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Test class that tests methods of the FKPMValueGetter class
 */
public class FKPMValueGetterTest {

    @Test
    /*
    This test tests a sunny day scenario
     */
    public void valueGetterSunnyday(){
        //Create new hashmap
        HashMap<String, HashMap<String, Integer>> filecontent = new HashMap<>();
        HashMap<String, Integer> experiment;

        experiment = new HashMap<>();
        experiment.put("WT1", 6376);
        experiment.put("WT2", 8382);
        filecontent.put("BSU00010",experiment);

        experiment = new HashMap<>();
        experiment.put("WT1", 4033);
        experiment.put("WT2", 6400);
        filecontent.put("BSU00020",experiment);

        FKPMValueGetter fkpmValueGetter = new FKPMValueGetter();
        List<Integer> expectedValueList = new ArrayList<>();
        expectedValueList.add(6376);
        expectedValueList.add(8382);
        expectedValueList.add(4033);
        expectedValueList.add(6400);
        assertTrue(expectedValueList.containsAll(fkpmValueGetter.getFKPMValues(filecontent)) && fkpmValueGetter.getFKPMValues(filecontent).containsAll(expectedValueList));
    }

    @Test(expected=IllegalArgumentException.class)
    /*
    This test tests a scenario when the retrieved hashmap is empty.
    This can happen when a empty or truncated file is uploaded.
    When the hashmap is empty, an Illegal Argument Exception is thrown
     */
    public void valueGetterEmpty() {
        HashMap<String, HashMap<String, Integer>> filecontent = new HashMap<>();
        HashMap<String, Integer> experiment = new HashMap<>();
        FKPMValueGetter fkpmValueGetter = new FKPMValueGetter();
        filecontent.put("", experiment);

        fkpmValueGetter.getFKPMValues(filecontent);
    }

}
